# README

## Workflow
metrics of workflow that are interesting are: 
- active connections 
- active consumers 
- active producers  
- release-tags 
- memstats

## Ingest
https://gist.github.com/nl5887/db8c1755f080bb1a52658822a17ecfd4
interesting:
- messages processed 
- application 
- connections*active 
- nofiles
- queues
- memstats & metrics


## Different 