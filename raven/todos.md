# To use `raven` Telegraf plugin:

* Fork telegraf
* ```cd ${fork}/plugins/inputs```.
* ```mkdir raven```.
* Paste `raven.go` and `raven_test.go` in that dir.
* Add to the file: `${fork}/go.mod`: 
```
replace github.com/influxdata/telegraf/plugins/inputs/raven => ./plugins/inputs/raven
```

* Add to the file `${fork}/plugins/inputs/all/all.go`:
```
	_ "github.com/influxdata/telegraf/plugins/inputs/raven"
```

* Add to custom config:
```toml
[[inputs.raven]]
    urls = [
        "http://localhost:8080/debug/vars"
    ]
    data_format = "json"
```

All other settings from the `http` plugin can be used.

* Build from `${fork}`:
```bash
make
```

* Launch with config:
```bash
{$fork}/telegraf --config ${configfile} --test
```

## Other / Bookmarks
[about memstats](https://golang.org/pkg/runtime/#MemStats)

[SO about adding plugin](https://stackoverflow.com/a/37521611)


[Contribution guide](https://github.com/influxdata/telegraf/blob/master/CONTRIBUTING.md)

[Inputs Plugin dev guide](https://github.com/influxdata/telegraf/blob/master/docs/INPUTS.md)
## Todo
[Fix](https://github.com/influxdata/telegraf/tree/release-1.14/plugins/parsers/json) JSON parsing with default_keys from config for plugin