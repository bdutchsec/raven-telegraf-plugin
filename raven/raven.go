package raven

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/influxdata/telegraf"
	"github.com/influxdata/telegraf/internal"
	"github.com/influxdata/telegraf/internal/tls"
	"github.com/influxdata/telegraf/plugins/inputs"
	"github.com/influxdata/telegraf/plugins/parsers"
)

type Raven struct {
	URLs            []string          `toml:"urls"`
	Method          string            `toml:"method"`
	Body            string            `toml:"body"`
	ContentEncoding string            `toml:"content_encoding"`
	Headers         map[string]string `toml:"headers"`

	// todo: add flag (boolean?) for workflow/ingest -> different parser used for each

	// HTTP basic auth credentials
	Username string `toml:"username"`
	Password string `toml:"password"`
	tls.ClientConfig

	SuccessStatusCodes []int             `toml:"success_status_codes"`
	Timeout            internal.Duration `toml:"timeout"`

	client *http.Client

	// The parser will automaticaly be set by Telegraf core code
	// As this plugin implements the ParserInput interface (i.e. the SetParser method)
	parser parsers.Parser
}

// todo: insert blank lines to sample
var sampleConfig = `
  ## One or more URLs from which to read formatted metrics
  urls = [
    "http://localhost/metrics"
  ]

  ## HTTP method
  # method = "GET"

  ## Optional HTTP headers
  # headers = {"X-Special-Header" = "Special-Value"}

  ## Optional HTTP Basic Auth Credentials
  # username = "username"
  # password = "pa$$word"

  ## HTTP entity-body to send with POST/PUT requests.
  # body = ""

  ## HTTP Content-Encoding for write request body, can be set to "gzip" to
  ## compress body or "identity" to apply no encoding.
  # content_encoding = "identity"

  ## Optional TLS Config
  # tls_ca = "/etc/telegraf/ca.pem"
  # tls_cert = "/etc/telegraf/cert.pem"
  # tls_key = "/etc/telegraf/key.pem"
  ## Use TLS but skip chain & host verification
  # insecure_skip_verify = false

  ## Amount of time allowed to complete the HTTP request
  # timeout = "5s"

  ## List of success status codes
  # success_status_codes = [200]

  ## Data format to consume.
  ## Each data format has its own unique set of configuration options, read
  ## more about them here:
  ## https://github.com/influxdata/telegraf/blob/master/docs/DATA_FORMATS_INPUT.md
  # data_format = "influx"
`

// SampleConfig returns default configuration of the Input
func (*Raven) SampleConfig() string {
	return sampleConfig
}

// Description
func (*Raven) Description() string {
	return "Reads formatted metrics from Raven workflow/ingest HTTP endpoint"
}

// Initializer - executed after package is imported
func (r *Raven) Init() error {
	tlsCfg, err := r.ClientConfig.TLSConfig()
	if err != nil {
		return err
	}

	r.client = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: tlsCfg,
			Proxy:           http.ProxyFromEnvironment,
		},
		Timeout: r.Timeout.Duration,
	}

	// Set default as [200]
	if len(r.SuccessStatusCodes) == 0 {
		r.SuccessStatusCodes = []int{200}
	}
	return nil
}

// Gather takes in an Telegraf accumulator and adds the metrics that the input gathers.
// This is called every "interval".
func (r *Raven) Gather(acc telegraf.Accumulator) error {
	var wg sync.WaitGroup
	for _, u := range r.URLs {
		wg.Add(1)
		go func(url string) {
			defer wg.Done()
			if err := r.gatherURL(acc, url); err != nil {
				acc.AddError(fmt.Errorf("[url=%s]: %s", url, err))
			}
		}(u)
	}
	wg.Wait()

	return nil
}

// SetParsers takes the data_format from the config and finds the right parser for that format
func (r *Raven) SetParser(parser parsers.Parser) {
	r.parser = parser
}

// gatherURL gathers data from particular URL
// Parameters:
//	acc : Telegraf accumulator to use
//	url : endpoint to send request to
//
// Returns:
//	error: Any error that may have occurred
func (r *Raven) gatherURL(
	acc telegraf.Accumulator,
	url string,
) error {
	//now := time.Now()
	body, err := makeRequestBodyReader(r.ContentEncoding, r.Body)
	if err != nil {
		return err
	}
	defer body.Close()

	request, err := http.NewRequest(r.Method, url, body)
	if err != nil {
		return err
	}

	// Set ContentEncoding in Header if required
	if r.ContentEncoding == "gzip" {
		request.Header.Set("Content-Encoding", "gzip")
	}

	for k, v := range r.Headers {
		if strings.ToLower(k) == "host" {
			request.Host = v
		} else {
			request.Header.Add(k, v)
		}
	}

	// Setup Auth if required
	if r.Username != "" || r.Password != "" {
		request.SetBasicAuth(r.Username, r.Password)
	}

	resp, err := r.client.Do(request)
	//fmt.Println(resp)  // todo: test print for response
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// check response codes, check for success codes given by config toml
	responseHasSuccessCode := false
	for _, statusCode := range r.SuccessStatusCodes {
		if resp.StatusCode == statusCode {
			responseHasSuccessCode = true
			break
		}
	}

	// Unsuccessful response; return error
	if !responseHasSuccessCode {
		return fmt.Errorf("received status code %d (%s), expected any value out of %v",
			resp.StatusCode,
			http.StatusText(resp.StatusCode),
			r.SuccessStatusCodes)
	}

	// Parse response body
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	//todo: reminder: b is now a bytearray
	// custom parser unmarshal, removes unused, then re-marshals
	output, err := customParser(b)
	if err != nil {
		return err
	}
	_ = output

	metrics, err := r.parser.Parse(output)
	if err != nil {
		return err
	}
	//todo: reminder: metrics is already parsed by telegraf JSON Parser
	//fmt.Println(metrics)

	//acc.AddFields('bla', )
	//fmt.Println(acc)


	for _, metric := range metrics {
		if !metric.HasTag("url") {
			metric.AddTag("url", url)
		}
		//fmt.Println(metric.Tags())
		//fmt.Println(metric.Time())
		//fmt.Println(metric.Fields())
		//now := time.Now().UTC()
		//fmt.Println(now)
		//fmt.Println("BLA \n")
		acc.AddFields(metric.Name(), metric.Fields(), metric.Tags(), metric.Time())
	}

	return nil
}

// customParser takes an byte array, outputs byte array and error.
// We unmarshal, remove two keys, marshal.
// Removes two hardcoded keys, due to incorrect formatting on Raven's side.
// todo: update/remove this func when workflow/ingest exposes differently
func customParser(input []byte) (output []byte, err error) {
	data := make(map[string]interface{})
	//var data map[string]interface{}
	err = json.Unmarshal(input, &data)
	if err != nil {
		return nil, err
	}
	// remove unrequired/unparsable metrics from data
	// todo: fix this after different exposing of metrics on workflow/ingest
	delete(data, "topics")
	delete(data, "subscriptions")
	return json.Marshal(data)

}

// Creates RequestBodyReader
func makeRequestBodyReader(contentEncoding, body string) (io.ReadCloser, error) {
	var reader io.Reader = strings.NewReader(body)
	if contentEncoding == "gzip" {
		rc, err := internal.CompressWithGzip(reader)
		if err != nil {
			return nil, err
		}
		return rc, nil
	}
	return ioutil.NopCloser(reader), nil
}

func init() {
	inputs.Add("raven", func() telegraf.Input {
		return &Raven{
			Timeout: internal.Duration{Duration: time.Second * 5},
			Method:  "GET",
		}
	})
}
