module raven-telegraf-plugin

go 1.14

require (
	github.com/alecthomas/units v0.0.0-20190717042225-c3de453c63f4
	github.com/influxdata/telegraf v1.14.3
	github.com/stretchr/testify v1.4.0
)

replace github.com/influxdata/telegraf/internal => /Users/bdutchsec/repos/GitHub/raven-telegraf-plugin/was_internal
